build:
	go build ./cmd/concurrent
	go build ./cmd/sequential

.PHONY: all test clean
test:
	go test -v ./test/...
