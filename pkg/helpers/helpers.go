package helpers

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// improved go Glob function. do not return hidden (dot prefixed) files
func Glob(pattern string) ([]string, error) {

	files, err := filepath.Glob(pattern) // this bastard is case sensitive and returns hidden files
	if err != nil {
		return files, err
	}
	onlyVisible := make([]string, 0, len(files))
	for _, fullpath := range files {
		_, filename := path.Split(fullpath)
		if strings.HasPrefix(filename, ".") {
			continue
		}
		onlyVisible = append(onlyVisible, fullpath)
	}
	return onlyVisible, err
}

// prefix file with the dot
func DotRename(filepath string) {
	dir, file := path.Split(filepath)
	log.Printf("Renaming %s\n", file)
	err := os.Rename(filepath, path.Join(dir, "."+file))
	if err != nil {
		log.Printf("Unable to rename file")
	}
}
