package helpers

import (
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/golang/protobuf/proto"
	"log"
	"strconv"
	"strings"
)

import "../appsinstalled"

// ======== Start AppsInstalled =========
type AppsInstalled struct {
	DeviceType string
	DeviceId   string
	Lon        float64
	Lat        float64
	Apps       []uint32
}

// need for unit test
func (app *AppsInstalled) Equals(other *AppsInstalled) bool {
	typeEq := app.DeviceType == other.DeviceType
	idEq := app.DeviceId == other.DeviceId
	lonEq := app.Lon == other.Lon
	latEq := app.Lat == other.Lat
	appsEq := len(app.Apps) == len(other.Apps)
	for i, value := range app.Apps {
		if value != other.Apps[i] {
			appsEq = false
			break
		}
	}
	return typeEq && idEq && lonEq && latEq && appsEq
}

func (app AppsInstalled) String() string {
	return fmt.Sprintf("%s.%s: (%f, %f) %v", app.DeviceType, app.DeviceId, app.Lon, app.Lat, app.Apps)
}

func ParseRecord(record []string) AppsInstalled {
	var deviceType, deviceId string
	var lat, lon float64
	var rawApps []string
	deviceType, deviceId = record[0], record[1]
	lat, err := strconv.ParseFloat(record[2], 64)
	if err != nil {
		log.Printf("Invalid latitude %s", record[2])
	}
	lon, err = strconv.ParseFloat(record[3], 64)
	if err != nil {
		log.Printf("Invalid longtitude %s", record[3])
	}
	rawApps = strings.Split(record[4], ",")
	apps := make([]uint32, len(rawApps))

	hasWrongApps := false
	for i, value := range rawApps {
		tmp, err := strconv.ParseUint(value, 10, 32)
		if err != nil {
			hasWrongApps = true
		} else {
			apps[i] = uint32(tmp)
		}
	}
	if hasWrongApps {
		log.Printf("Not all user apps are digits in %s.%s", deviceType, deviceId)
	}
	return AppsInstalled{
		DeviceType: deviceType,
		DeviceId:   deviceId,
		Lat:        lat,
		Lon:        lon,
		Apps:       apps,
	}
}

// ========== End Apps Installed =============

// ========== Start Memcache related helpers ==========

type MemcClient struct {
	*memcache.Client
	Address string
}

func MemcacheClientsFactory(dry bool, idfa, gaid, adid, dvid string) map[string]MemcClient {

	clients := make(map[string]MemcClient)

	if !dry {
		clients["idfa"] = MemcClient{memcache.New(idfa), idfa}
		clients["gaid"] = MemcClient{memcache.New(gaid), gaid}
		clients["adid"] = MemcClient{memcache.New(adid), adid}
		clients["dvid"] = MemcClient{memcache.New(dvid), dvid}
	} else {
		clients["idfa"] = MemcClient{nil, idfa}
		clients["gaid"] = MemcClient{nil, gaid}
		clients["adid"] = MemcClient{nil, adid}
		clients["dvid"] = MemcClient{nil, dvid}
	}

	return clients
}

func MemcacheSave(deviceType, deviceId string, bytes []byte, client MemcClient, dry bool) error {

	if dry {
		log.Printf("%s:%s -> %s\n", deviceType, deviceId, client.Address)
		return nil
	}

	item := &memcache.Item{
		Key:   fmt.Sprintf("%s:%s", deviceType, deviceId),
		Value: bytes,
	}
	return client.Set(item)
}

// ======= Start Results =======
// collecting processing results

type Results struct {
	Success uint32
	Failed  uint32
}

func (r *Results) ErrorRate() float64 {
	return float64(r.Failed) / float64(r.Success)
}

func (r *Results) IncSuccess() {
	r.Success += 1
}

func (r *Results) IncFailed() {
	r.Failed += 1
}

// ======= End Results =======

// some helper functions

// converts AppsInstalled instance into protobuf packed byte array
func Marshal(app AppsInstalled) ([]byte, error) {
	userApp := &appsinstalled.UserApps{
		Lat:  proto.Float64(app.Lat),
		Lon:  proto.Float64(app.Lon),
		Apps: app.Apps,
	}
	return proto.Marshal(userApp)
}
