package main

import (
	"github.com/golang/protobuf/proto"
	"strings"
	"testing"
)
import "../../internal/pkg/appsinstalled"
import "../../internal/pkg/helpers"

func TestParseRecord(t *testing.T) {
	sample := "idfa\t1rfw452y52g2gq4g\t55.55\t42.42\t1423,43,567,3,7,23\ngaid\t7rfw452y52g2gq4g\t55.55\t42.42\t7423,424"
	expected := []helpers.AppsInstalled{
		{
			DeviceType: "idfa",
			DeviceId:   "1rfw452y52g2gq4g",
			Lat:        float64(55.55),
			Lon:        float64(42.42),
			Apps:       []uint32{1423, 43, 567, 3, 7, 23},
		},
		{
			DeviceType: "gaid",
			DeviceId:   "7rfw452y52g2gq4g",
			Lat:        float64(55.55),
			Lon:        float64(42.42),
			Apps:       []uint32{7423, 424},
		},
	}
	for i, line := range strings.Split(sample, "\n") {
		record := helpers.ParseRecord(strings.Split(line, "\t"))
		if !record.Equals(&expected[i]) {
			t.Errorf("%s => %v, expected %v", line, record, expected[i])
		}
	}
}

func TestProto(t *testing.T) {
	apps := []helpers.AppsInstalled{
		{
			DeviceType: "idfa",
			DeviceId:   "1rfw452y52g2gq4g",
			Lat:        float64(55.55),
			Lon:        float64(42.42),
			Apps:       []uint32{1423, 43, 567, 3, 7, 23},
		},
		{
			DeviceType: "gaid",
			DeviceId:   "7rfw452y52g2gq4g",
			Lat:        float64(55.55),
			Lon:        float64(42.42),
			Apps:       []uint32{7423, 424},
		},
	}
	for _, app := range apps {
		bytes, err := helpers.Marshal(app)
		if err != nil {
			t.Fatal("Error during marshalling")
		}
		userApp := &appsinstalled.UserApps{}
		err = proto.Unmarshal(bytes, userApp)
		if err != nil {
			t.Fatal("Error during unmarshalling")
		}
		latEq := app.Lat == userApp.GetLat()
		lonEq := app.Lon == userApp.GetLon()
		unmarshalledApps := userApp.GetApps()
		appsEq := len(app.Apps) == len(unmarshalledApps)
		if appsEq {
			for i, v := range app.Apps {
				if v != unmarshalledApps[i] {
					appsEq = false
					break
				}
			}
		}

		if !(latEq && lonEq && appsEq) {
			t.Errorf("%v => %v", app, userApp)
		}
	}
}
