package main

import (
	"bufio"
	"compress/gzip"
	"flag"
	"log"
	"os"
	"strings"
)

import (
	internalHelpers "../../internal/pkg/helpers"
	"../../pkg/helpers"
)

type Config struct {
	AcceptableErrorRate float64
	MemcRetries         int
	MemcTimeout         float64
	MemcBackoff         float64
	Workers             int
}

var config = Config{}

func init() {
	config.AcceptableErrorRate = 0.01
}

func processFile(filename string, memcacheClients map[string]internalHelpers.MemcClient, config Config, dry bool) {
	const FIELDS = 5
	file, err := os.Open(filename)

	if err != nil {
		log.Fatal(err)
		return
	}
	defer file.Close()
	reader, err := gzip.NewReader(file)
	if err != nil {
		log.Print(err)
		return
	}

	result := internalHelpers.Results{
		Failed:  0,
		Success: 0,
	}

	scanner := bufio.NewScanner(reader)

	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), "\n")
		record := strings.Split(line, "\t")
		if len(record) != FIELDS {
			result.IncFailed()
			log.Printf("Invalid line in %s: %s\n", filename, line)
			continue
		}
		app := internalHelpers.ParseRecord(record)
		protobuffed, err := internalHelpers.Marshal(app)
		if err != nil {
			result.IncFailed()
			log.Printf("Error during marshalling line in %s: %s\n", filename, line)
			continue
		}
		mc := memcacheClients[app.DeviceType]
		err = internalHelpers.MemcacheSave(app.DeviceType, app.DeviceId, protobuffed, mc, dry)
		if err != nil {
			result.IncFailed()
		}
		result.IncSuccess()
	}
	errorRate := result.ErrorRate()
	if errorRate < config.AcceptableErrorRate {
		log.Printf("Processed %d records, acceptable error rate(%.2f). Successful load",
			result.Failed+result.Success, errorRate)
		helpers.DotRename(filename)
	} else {
		log.Printf("High error rate (%.2f >= %.2f). Failed load", errorRate, 0.01)
	}
}

func main() {

	var dry bool
	var logfile, pattern, idfa, gaid, adid, dvid string

	flag.BoolVar(&dry, "dry", false, "Do not store values in memcached")
	flag.StringVar(&logfile, "log", "", "Logs file")
	flag.StringVar(&pattern, "pattern", "/data/appsinstalled/*.tsv.gz", "Filename pattern")
	flag.StringVar(&idfa, "idfa", "127.0.0.1:33013", "memcached address for idfa")
	flag.StringVar(&gaid, "gaid", "127.0.0.1:33014", "memcached address for gaid")
	flag.StringVar(&adid, "adid", "127.0.0.1:33015", "memcached address for adid")
	flag.StringVar(&dvid, "dvid", "127.0.0.1:33016", "memcached address for dvid")
	flag.Parse()

	if len(strings.Trim(logfile, "\n\t ")) != 0 {
		file, err := os.OpenFile(logfile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
		if err != nil {
			log.Fatal("Could not open logfile") // print message and exit(1)
		}
		defer file.Close()
		log.SetOutput(file)
	}

	memcacheClients := internalHelpers.MemcacheClientsFactory(dry, idfa, gaid, adid, dvid)

	log.Printf("Searching for files with pattern \"%s\"", pattern)
	files, err := helpers.Glob(pattern)
	if err != nil {
		log.Fatalf("Incorrect pattern \"%s\"", pattern)
		return
	}
	for _, file := range files {
		log.Printf("Processing file \"%s\"...", file)
		processFile(file, memcacheClients, config, dry)
	}
}
