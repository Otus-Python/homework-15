package main

import (
	"bufio"
	"compress/gzip"
	"flag"
	"log"
	"os"
	"runtime"
	"strings"
)

import (
	internalHelpers "../../internal/pkg/helpers"
	"../../pkg/helpers"
)

const FIELDS = 5 // fields number in a valid log line

type FileResult struct {
	filename string
	success  bool
}

type Config struct {
	AcceptableErrorRate float64
	Workers             int // simultaneously processed files
	Routines            int // routines for loading in memcached per worker
}

var config = Config{}

func init() {
	config.AcceptableErrorRate = 0.01
	config.Workers = runtime.NumCPU()
	config.Routines = 25
}

// connections per server count = Workers * Routines

// need for passing closure function into workers
type memcFactory func() map[string]internalHelpers.MemcClient

// worker function
func worker(filename string,
	resultsChannel chan<- FileResult,
	factory memcFactory,
	semaphore <-chan int,
	config Config, dry bool) {

	// release "semaphore" on exit
	defer func() {
		<-semaphore
	}()

	jobs := make(chan internalHelpers.AppsInstalled, 1000)
	results := make(chan internalHelpers.Results, config.Routines)

	// run routines
	for i := 0; i < config.Routines; i++ {
		memcacheClients := factory()
		go serializeAndLoadApps(jobs, results, memcacheClients, dry)
	}

	result := internalHelpers.Results{
		Failed:  0,
		Success: 0,
	}

	file, err := os.Open(filename)

	if err != nil {
		log.Fatal(err)
		return
	}
	defer file.Close()
	reader, err := gzip.NewReader(file)
	if err != nil {
		log.Print(err)
		return
	}

	// read the file and pass the jobs to the channel
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), "\n")
		record := strings.Split(line, "\t")
		if len(record) != FIELDS {
			result.IncFailed()
			log.Printf("Invalid line in %s: %s\n", filename, line)
			continue
		}
		app := internalHelpers.ParseRecord(record)
		jobs <- app
	}
	close(jobs)

	// aggregate results
	for i := 0; i < config.Routines; i++ {
		routineResult := <-results
		result.Success += routineResult.Success
		result.Failed += routineResult.Failed
	}

	// print results and pass it into results channel
	errorRate := result.ErrorRate()
	if errorRate < config.AcceptableErrorRate {
		log.Printf("Processed %d records in %s, acceptable error rate(%.2f). Successful load",
			result.Failed+result.Success, filename, errorRate)
		resultsChannel <- FileResult{filename: filename, success: true}
	} else {
		log.Printf("High error rate (%.2f >= %.2f) in %s. Failed load",
			errorRate,  config.AcceptableErrorRate, filename)
		resultsChannel <- FileResult{filename: filename, success: false}
	}
}

// fetch apps from channel, serialize into protobuf and pass to memcached
// count intermediate result and pass it to the channel
func serializeAndLoadApps(
	jobs <-chan internalHelpers.AppsInstalled,
	results chan internalHelpers.Results,
	memcacheClients map[string]internalHelpers.MemcClient,
	dry bool) {

	result := internalHelpers.Results{
		Failed:  0,
		Success: 0,
	}

	for app := range jobs {
		protobuffed, err := internalHelpers.Marshal(app)
		if err != nil {
			result.IncFailed()
			log.Println("Error during marshalling")
			continue
		}
		mc := memcacheClients[app.DeviceType]
		err = internalHelpers.MemcacheSave(app.DeviceType, app.DeviceId, protobuffed, mc, dry)
		if err != nil {
			result.IncFailed()
		}
		result.IncSuccess()
	}
	results <- result
}

func main() {

	var dry bool
	var logfile, pattern, idfa, gaid, adid, dvid string

	flag.BoolVar(&dry, "dry", false, "Do not store values in memcached")
	flag.StringVar(&logfile, "log", "", "Logs file")
	flag.StringVar(&pattern, "pattern", "/data/appsinstalled/*.tsv.gz", "Filename pattern")
	flag.StringVar(&idfa, "idfa", "127.0.0.1:33013", "memcached address for idfa")
	flag.StringVar(&gaid, "gaid", "127.0.0.1:33014", "memcached address for gaid")
	flag.StringVar(&adid, "adid", "127.0.0.1:33015", "memcached address for adid")
	flag.StringVar(&dvid, "dvid", "127.0.0.1:33016", "memcached address for dvid")
	flag.Parse()

	// setup logging
	if len(strings.Trim(logfile, "\n\t ")) != 0 {
		file, err := os.OpenFile(logfile, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
		if err != nil {
			log.Fatal("Could not open logfile") // print message and exit(1)
		}
		defer file.Close()
		log.SetOutput(file)
	}

	// creating closure for memcache factory
	factory := func() map[string]internalHelpers.MemcClient {
		return internalHelpers.MemcacheClientsFactory(dry, idfa, gaid, adid, dvid)
	}

	log.Printf("Searching for files with pattern \"%s\"", pattern)
	files, err := helpers.Glob(pattern)
	if len(files) == 0 {
		log.Print("No files for processing")
	}
	if err != nil {
		log.Fatalf("Incorrect pattern \"%s\"", pattern)
		return
	}

	resultsChannel := make(chan FileResult, len(files))
	semaphore := make(chan int, config.Workers)

	for _, file := range files {
		semaphore <- 1
		log.Printf("Processing file \"%s\"...", file)
		go worker(file, resultsChannel, factory, semaphore, config, dry)
	}

	results := make(map[string]bool)

	for i := 0; i < len(files); i++ {
		result := <-resultsChannel
		results[result.filename] = result.success
	}

	// rename processed files
	for _, file := range files {
		if results[file] {
			helpers.DotRename(file)
		}
	}
}
